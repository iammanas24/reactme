/* Change this file to get your personal Portfolio */

// To change portfolio colors globally go to the  _globalColor.scss file

// Summary And Greeting Section

import emoji from "react-easy-emoji";

const illustration = {
  animated: true // set to false to use static SVG
};

const greeting = {
  username: "Manas Ranjan Pati",
  title: "Namaste, I'm Manas",
  subTitle: emoji(
    "A passionate Full Stack Software Developer 🚀 having an experience of building Web and Mobile applications with Python / Django / RestAPI / React and some other cool libraries and frameworks."
  ),
  resumeLink:
    "https://drive.google.com/file/d/1sTzuNeO0Gy5AxTLEvlz_Uu-M0W-m4cuv/view?usp=sharing",
  displayGreeting: true // Set false to hide this section, defaults to true
};

// Social Media Links

const socialMediaLinks = {
  github: "https://github.com/Manasranjanpati",
  instagram: "https://www.instagram.com/manasranjanpati/",
  twitter: "https://twitter.com/iam_manas",
  linkedin: "https://linkedin.com/in/manas-ranjan-pati-0969009a/",
  gmail: "iammanas24@gmail.com",
  gitlab: "https://gitlab.com/",
  // facebook: "https://www.facebook.com/",
  medium: "https://medium.com/",

  // stackoverflow: "https://stackoverflow.com/users/10422806/saad-pasta",
  // Instagram and Twitter are also supported in the links!
  display: true // Set true to display this section, defaults to false
};

// Skills Section

const skillsSection = {
  title: "What I do",
  subTitle: "CRAZY FULL STACK DEVELOPER WHO WANTS TO EXPLORE EVERY TECH STACK",
  skills: [
    emoji(
      "⚡ Developing applications using Python, Django, Flask, RestFramework. Working as a Python Consultant"
    ),
    emoji("⚡Ability to research and gain good working knowledge of technologies relevant to the strategic direction of the requirements of the project and a suitable tech stack."),
    emoji(
      "⚡ Working on third party services like AWS, Docker, Firebase"
    )
  ],

  /* Make Sure to include correct Font Awesome Classname to view your icon
https://fontawesome.com/icons?d=gallery */

  softwareSkills: [
    {
      skillName: "python",
      fontAwesomeClassname: "fab fa-python"
    },
    {
      skillName: "Django",
      fontAwesomeClassname: "fab fa-dyalog"
    },
    {
      skillName: "DRF",
      fontAwesomeClassname: "fab fa-python"
    },
    {
      skillName: "Html5",
      fontAwesomeClassname: "fab fa-html5"
    },
    {
      skillName: "docker",
      fontAwesomeClassname: "fab fa-docker"
    },
    {
      skillName: "Data Analysis",
      fontAwesomeClassname: "fas fa-table"
    },
    {
      skillName: "css3",
      fontAwesomeClassname: "fab fa-css3-alt"
    },  
    {
      skillName: "JavaScript",
      fontAwesomeClassname: "fab fa-js"
    },
    {
      skillName: "sql-database",
      fontAwesomeClassname: "fas fa-database"
    },
    {
      skillName: "aws",
      fontAwesomeClassname: "fab fa-aws"
    },
    {
      skillName: "firebase",
      fontAwesomeClassname: "fas fa-fire"
    },
  
    
  ],
  display: true // Set false to hide this section, defaults to true
};

// Education Section

const educationInfo = {
  display: true, // Set false to hide this section, defaults to true
  schools: [
    {
      schoolName: "Biju Pattnaik University of Technology",
      logo: require("./assets/images/BPUT.png"),
      subHeader: "BE in Mechanical Engineering",
      duration: "April 2016 - April 2019",
      desc: "Completed degree program after my Diploma in Mechanical Engineering",
      descBullets: [
        "Started learning programming when I introduced to IOT/ML during my collage days",
        "I never liked to study instead spending more time in Labs"
      ]
    },
    // {
    //   schoolName: "Stanford University",
    //   logo: require("./assets/images/BPUT.png"),
    //   subHeader: "Bachelor of Science in Computer Science",
    //   duration: "September 2013 - April 2017",
    //   desc: "Ranked top 10% in the program. Took courses about Software Engineering, Web Security, Operating Systems, ...",
    //   descBullets: ["Lorem ipsum dolor sit amet, consectetur adipiscing elit"]
    // }
  ]
};

// Your top 3 proficient stacks/tech experience

const techStack = {
  viewSkillBars: true, //Set it to true to show Proficiency Section
  experience: [
    {
      Stack: "Backend Development using Python/Django/DRF/Flask", //Insert stack or technology you have experience in
      progressPercentage: "90%" //Insert relative proficiency in percentage
    },
    {
      Stack: "Devops",
      progressPercentage: "70%"
    },
    {
      Stack: "Programming",
      progressPercentage: "60%"
    }
  ],
  displayCodersrank: false // Set true to display codersrank badges section need to changes your username in src/containers/skillProgress/skillProgress.js:17:62, defaults to false
};

// Work experience section

const workExperiences = {
  display: true, //Set it to true to show workExperiences Section
  experience: [

   {
      role: "Software Engineer",
      company: "Tata Consultancy Services",
      companylogo: require("./assets/images/tcs.png"),
      date: "Jan 2022 – Present",
      desc: "Joined as a Software Consultant",
      descBullets: [
        "Working with client Ericsson",
      ]
    },

    {
      role: "Software Engineer",
      company: "Ojas Innovative Technologies",
      companylogo: require("./assets/images/ojas.png"),
      date: "March 2021 – Present",
      desc: "Joined as Software Enginner to work on both Internal SDLC and client Python Consulting ",
      descBullets: [
        "Trained almost 23 Interns on Python/Django Framework",
        "Working with clients like Quadratyx/Soothsayer Analytics for SABIC DS/DE projects."
      ]
    },
    {
      role: "Software Developer",
      company: "Micropyramid Informatics Pvt Ltd",
      companylogo: require("./assets/images/mplogo.png"),
      date: "May 2019 – Feb 2021",
      desc: "Worked as a Software Developer",
      descBullets: [
        "Worked on Python/Django/Flask/DRF",
        "Exploring, learning, doing R&D, implementing and finding solutions",
        "Worked on cloud tools like Docker,AWS, CI/CD."
      ]
    }
  ]
};


/* Your Open Source Section to View Your Github Pinned Projects
To know how to get github key look at readme.md */

const openSource = {
  showGithubProfile: "true", // Set true or false to show Contact profile using Github, defaults to true
  display: true // Set false to hide this section, defaults to true
};

// Some big projects you have worked on

const bigProjects = {
  title: "Big Projects",
  subtitle: "SOME CLIENT COMPANIES THAT I HELPED TO CREATE THEIR TECH",
  projects: [
    {
      image: require("./assets/images/sabic.png"),
      projectName: "SABIC DPP",
      projectDesc: "With Sabic, I work as a Data Engineer, Building models for Price Prediction Forecasting",
      footerLink: [
        {
          name: "Visit Website",
          url: "https://www.sabic.com/en"
        }
        //  you can add extra buttons here.
      ]
    },
    {
      image: require("./assets/images/quadtryx.png"),
      projectName: "QUADTRYX",
      projectDesc: "Quadtryx is my client where we work on Data Science and ML ",
      footerLink: [
        {
          name: "Visit Website",
          url: "https://quadratyx.com/"
        }
      ]
    }
  ],
  display: true // Set false to hide this section, defaults to true
};

// Achievement Section
// Include certificates, talks etc

const achievementSection = {
  title: emoji("Achievements And Certifications 🏆 "),
  subtitle:
    "Achievements, Certifications, Award Letters and Some Cool Stuff that I have done !",

  achievementsCards: [
    {
      title: "Docker Mastery",
      subtitle:
        "Completed Certification on Docker with Kubernetes",
      image: require("./assets/images/udemy.png"),
      footerLink: [
        {
          name: "Certification",
          url: "https://udemy-certificate.s3.amazonaws.com/image/UC-578ee68f-a6e9-423f-8f1e-f580ffd15f6a.jpg?v=1604670450000"
        },
        // {
        //   name: "Award Letter",
        //   url: "https://drive.google.com/file/d/0B7kazrtMwm5dekxBTW5hQkg2WXUyR3QzQmR0VERiLXlGRVdF/view?usp=sharing"
        // },
        // {
        //   name: "Google Code-in Blog",
        //   url: "https://opensource.googleblog.com/2019/01/google-code-in-2018-winners.html"
        // }
      ]
    },
    {
      title: "Rapidminer Data Engineering Professional",
      subtitle:
        "Certification on Data Engineering Professional implemented on SABIC DPP",
      image: require("./assets/images/rapidminer.jpg"),
      footerLink: [
        {
          name: "Rapidminer Certification",
          url: "https://drive.google.com/file/d/1y4e9DNR-09592rzEE5yL6rxczYyi23Ng/view?usp=sharing"
        },
        {
          name: "Badge",
          url: "https://openbadgepassport.com/app/badge/info/380502"
        }
      ]
    },

    // {
    //   title: "PWA Web App Developer",
    //   subtitle: "Completed Certifcation from SMIT for PWA Web App Development",
    //   image: require("./assets/images/pwaLogo.webp"),
    //   footerLink: [
    //     {name: "Certification", url: ""},
    //     {
    //       name: "Final Project",
    //       url: "https://pakistan-olx-1.firebaseapp.com/"
    //     }
    //   ]
    // }
  ],
  display: true // Set false to hide this section, defaults to true
};

// Blogs Section

const blogSection = {
  title: "Blogs",
  subtitle:
    "With Love for Developing cool stuff, I love to write and teach others what I have learnt.",

  blogs: [
    {
      url: "https://manasblog.netlify.app/posts/",
      title: "My Blogs on Technology and Stacks",
      description:
        "Weekends can be spend on Learning and Teaching new things. I think the best way to learn anything is to Teach others the same thing."
    },
    {
      url: "https://manasinfos.home.blog/",
      title: "My Blogs on Humanity",
      description:
        "I love to write on social issues in my free time"
    }
  ],
  display: true // Set false to hide this section, defaults to true
};

// Talks Sections

const talkSection = {
  title: "TALKS",
  subtitle: emoji(
    "I LOVE TO SHARE MY LIMITED KNOWLEDGE AND GET A SPEAKER BADGE 😅"
  ),

  talks: [
    {
      title: "Django Sessions by Manas",
      subtitle: "Training on Python Frameworks like Django, Flask to Interns",
      slides_url: "https://drive.google.com/file/d/10G1yQ1-USxT9v5hZIxmoDLj8U7wJWS4S/view?usp=sharing",
      event_url: "#"
    }
  ],
  display: true // Set false to hide this section, defaults to true
};

// Podcast Section

const podcastSection = {
  title: emoji("Podcast 🎙️"),
  subtitle: "I LOVE TO TALK ABOUT MYSELF AND TECHNOLOGY",

  // Please Provide with Your Podcast embeded Link
  podcast: [
    "https://anchor.fm/codevcast/embed/episodes/DevStory---Saad-Pasta-from-Karachi--Pakistan-e9givv/a-a15itvo"
  ],
  display: false // Set false to hide this section, defaults to true
};

const contactInfo = {
  title: emoji("Contact Me ☎️"),
  subtitle:
    " Lets connect, I love to connect with techies Or we can collaborate too !!!",
  number: "+91-9439034452",
  email_address: "iammanas24@gmail.com"
};

// Twitter Section

const twitterDetails = {
  userName: "iam_manas", //Replace "twitter" with your twitter username without @
  display: true // Set true to display this section, defaults to false
};

export {
  illustration,
  greeting,
  socialMediaLinks,
  skillsSection,
  educationInfo,
  techStack,
  workExperiences,
  openSource,
  bigProjects,
  achievementSection,
  blogSection,
  talkSection,
  podcastSection,
  contactInfo,
  twitterDetails
};
